package mainMenuUi;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class AlertBox extends Application {

    public static Stage classStage = new Stage();

    public void start(Stage primaryStage) {

        classStage = primaryStage;

        HBox titleBox = new HBox();
        titleBox.setAlignment(Pos.CENTER);
        titleBox.setId("Box");

        GridPane box = new GridPane();
        box.setPadding(new Insets(10, 10, 10, 10));
        box.setVgap(5);
        box.setHgap(5);
        box.setAlignment(Pos.CENTER);

        Label title = new Label();
        title.setText("Exit Application");
        title.setId("title");
        titleBox.getChildren().add(title);

        HBox btnBox = new HBox();
        btnBox.setAlignment(Pos.CENTER);

        Button btnYes = new Button();
        btnYes.setText("Yes");
        btnYes.setOnAction(e -> System.exit(0));

        Button btnNo = new Button();
        btnNo.setText("No");
        btnNo.setOnAction(e -> primaryStage.close());

        box.add(btnYes, 1, 5);
        box.add(btnNo, 2, 5);

        btnBox.getChildren().add(box);

        VBox vBox = new VBox();
        vBox.setMinSize(300, 150);
        vBox.getChildren().addAll(titleBox, btnBox);
        vBox.setId("Box2");

        Scene scene = new Scene(vBox);
        String css = this.getClass().getResource("style.css").toExternalForm();
        primaryStage.setScene(scene);
        scene.getStylesheets().add(css);
        primaryStage.show();
    }

    public static void main(String[] args) {

        launch(args);
    }
}
