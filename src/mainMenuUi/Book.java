package mainMenuUi;

@SuppressWarnings("ALL")
public class Book {

    private String title;
    private int isbn;
    private String authorName;
    private double edition;
    private String category;
    private int BookID;

    public Book(String title, int isbn, String authorName, double edition, String category, int BookID) {

        this.title = title;
        this.isbn = isbn;
        this.authorName = authorName;
        this.edition = edition;
        this.category = category;
        this.BookID = BookID;
    }

    public Book() {

        this.title = "";
        this.isbn = 0;
        this.authorName = "";
        this.edition = 0.0;
        this.category = "";
        this.BookID = 0;
    }

    public String getTitle() {

        return title;
    }

    public void setTitle(String title) {

        this.title = title;
    }


    public String getAuthorName() {

        return authorName;
    }

    public void setAuthorName(String authorName) {

        this.authorName = authorName;
    }


    public int getIsbn() {

        return isbn;
    }

    public void setIsbn(int isbn) {

        this.isbn = isbn;
    }

    public double getEdition() {

        return edition;
    }

    public void setEdition(double edition) {

        this.edition = edition;
    }

    public String getCategory() {
        
        return category;
    }

    public void setCategory(String category) {

        this.category = category;
    }

    public int getBookID() {

        return BookID;
    }

    public void setBookID(int BookID) {

        this.BookID = BookID;
    }
}
