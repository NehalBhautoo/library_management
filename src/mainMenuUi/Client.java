package mainMenuUi;

public class Client {

    private String name;
    private String surName;
    private int phoneNumber;
    private String emails;
    private int nid;

    public Client(String name, String surName, int phoneNumber, String emails, int nid) {

        this.name = name;
        this.surName = surName;
        this.phoneNumber = phoneNumber;
        this.emails = emails;
        this.nid = nid;
    }

    public Client() {

        this.name = "";
        this.surName = "";
        this.phoneNumber = 0;
        this.emails = "";
        this.nid = 0;
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {

        this.name = name;
    }

    public String getSurName() {

        return surName;
    }

    public void setSurName(String surName) {

        this.surName = surName;
    }

    public int getPhoneNumber() {

        return phoneNumber;
    }

    public void setPhoneNumber(int phoneNumber) {

        this.phoneNumber = phoneNumber;
    }

    public String getEmails() {

        return emails;
    }

    public void setEmails(String emails) {

        this.emails = emails;
    }

    public int getNid() {

        return nid;
    }

    public void setNid(int nid) {

        this.nid = nid;
    }
}
