package mainMenuUi;

public class Issued {

    private String title;
    private String name;
    private String day;
    private String month;
    private String year;

    public Issued(String title, String name, String day, String month, String year) {

        this.title = title;
        this.name = name;
        this.day = day;
        this.month = month;
        this.year = year;

    }

    public Issued() {

        this.title = "";
        this.name = "";
        this.day = "";
        this.month = "";
        this.year = "";
    }

    public String getTitle() {

        return title;
    }

    public void setTitle(String title) {

        this.title = title;
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {

        this.name = name;
    }


    public String getDay() {

        return day;
    }

    public void setDay(String day) {

        this.day = day;
    }

    public String getMonth() {

        return month;
    }

    public void setMonth(String month) {

        this.month = month;
    }

    public String getYear() {

        return year;
    }

    public void setYear(String year) {

        this.year = year;
    }
}
