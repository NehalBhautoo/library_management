package mainMenuUi;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Screen;
import javafx.stage.Stage;

import javax.swing.*;
import java.io.*;

import static javafx.geometry.Pos.TOP_CENTER;

@SuppressWarnings("ALL")
public class Main_Menu extends Application {

    private BorderPane layout;
    private Scene scene;
    private TextField titleField, numField, authorField, editionField, idField, searchField, searchField2, firstNameField, surNameField,
                      phoneField, mailField, nidField, book1TitleField, isbn1Field, book2TitleField, isbn2Field, book3TitleField, isbn3Field,
                      book4TitleField, isbn4Field, book5TitleField, isbn5Field, bookIDfield, memberIDfield, bookTitlefield, issueDateField, memberName, bookID, newtitleField,
                      newidField, newauthorField, neweditionField, newisbn1Field;

    ComboBox<String> categoryBox , comboBox, sortComboBox, day, month, year;
    private Stage stage, listWindow, addNewBook, listClient, issue_Book, listIssuedBook, removeBook, findBook;
    private TableView<Book> tableBook;
    private TableView<Client> tableClient;
    private TableView<Issued> tableIssue;

    public static void main(String[] args) {

        launch();
    }

    @Override
    public void start(Stage primaryStage) {
        stage = primaryStage;
        layout = new BorderPane();

        //Give Root Node a CSS ID Attribute
        layout.setId("appContainer");

        //Set Scene Properties.
        setSceneProperties();

        //Build Demo App Layout
        try {
            buildLeft();
        } catch (Exception e) {}
        buildTop();
        table();
        layout.setCenter(tableBook);

        //Set a few properties of our Application Window
        stage.setScene(scene);
        stage.setTitle("Main");
        stage.show();
    }

    /**
     * buildLeft. This method builds the Left Region of BorderPane.
     * This is a BorderPane with VBox containing buttons and border.
     */

    private void buildLeft() throws Exception{

        BorderPane leftLayout = new BorderPane();

        // Create a border-right effect using a Label.
        Label divider = new Label();
        divider.setId("divider1");
        divider.setPrefWidth(1);
        divider.setMinHeight(Screen.getPrimary().getBounds().getHeight());
        leftLayout.setRight(divider);

        VBox buttonBox = new VBox();
        buttonBox.setMinWidth(150);

        //Set Alignment of Buttons in VBox Container.
        buttonBox.setAlignment(TOP_CENTER);

        //Give VBox a CSS ID
        buttonBox.setId("buttonMenuContainer");

        //Create some vertical spacing between buttons
        buttonBox.setSpacing(20);

        ImageView viewBookIcon = new ImageView(new Image(new FileInputStream("icons/viewBook.png")));
        viewBookIcon.setFitHeight(25);
        viewBookIcon.setFitWidth(25);
        Button btnViewBooks = new Button("View Books", viewBookIcon);
        btnViewBooks.setAlignment(Pos.BASELINE_LEFT);

        //Set All Buttons to the same size.
        btnViewBooks.setMaxWidth(Double.MAX_VALUE);

        //Add Click Event.
        btnViewBooks.setOnAction(e -> layout.setCenter(tableBook));

        ImageView addBookIcon = new ImageView(new Image(new FileInputStream("icons/addBook.png")));
        addBookIcon.setFitHeight(25);
        addBookIcon.setFitWidth(25);
        Button btnAddBook = new Button("Add book", addBookIcon);
        btnAddBook.setAlignment(Pos.BASELINE_LEFT);
        btnAddBook.setMaxWidth(Double.MAX_VALUE);
        btnAddBook.setOnAction(e -> {

            buttonAdd();

        });

        ImageView viewMemberIcon = new ImageView(new Image(new FileInputStream("icons/id-card.png")));
        viewMemberIcon.setFitHeight(25);
        viewMemberIcon.setFitWidth(25);
        Button btnviewCustomer = new Button("View Member", viewMemberIcon);
        btnviewCustomer.setMaxWidth(Double.MAX_VALUE);
        btnviewCustomer.setAlignment(Pos.BASELINE_LEFT);
        btnviewCustomer.setOnAction(e -> {

            clientTable();
            layout.setCenter(tableClient);
        });

        ImageView addMemberIcon = new ImageView(new Image(new FileInputStream("icons/id.png")));
        addMemberIcon.setFitHeight(25);
        addMemberIcon.setFitWidth(25);
        Button btnAddMember = new Button("Add Member", addMemberIcon);
        btnAddMember.setMaxWidth(Double.MAX_VALUE);
        btnAddMember.setAlignment(Pos.BASELINE_LEFT);
        btnAddMember.setOnAction(e -> addClient());

        ImageView addIssueIcon = new ImageView(new Image(new FileInputStream("icons/issueBook.png")));
        addIssueIcon.setFitHeight(25);
        addIssueIcon.setFitWidth(25);
        Button btnIssueBook = new Button("Issue Book", addIssueIcon);
        btnIssueBook.setAlignment(Pos.BASELINE_LEFT);
        btnIssueBook.setMaxWidth(Double.MAX_VALUE);
        btnIssueBook.setOnAction(e -> {
            try {
                issueBook();
            } catch (Exception ex) { }
        });

        ImageView viewIssueIcon = new ImageView(new Image(new FileInputStream("icons/viewBook.png")));
        viewIssueIcon.setFitHeight(25);
        viewIssueIcon.setFitWidth(25);
        Button btnViewIssuedeBook = new Button("View Issued Book", viewIssueIcon);
        btnViewIssuedeBook.setMaxWidth(Double.MAX_VALUE);
        btnViewIssuedeBook.setAlignment(Pos.BASELINE_LEFT);
        btnViewIssuedeBook.setOnAction(e -> {

            issuedTable();
            layout.setCenter(tableIssue);

        });

        ImageView removeBookIcon = new ImageView(new Image(new FileInputStream("icons/removebook.png")));
        removeBookIcon.setFitHeight(25);
        removeBookIcon.setFitWidth(25);
        Button btnRemoveBook = new Button("Remove Book", removeBookIcon);
        btnRemoveBook.setMaxWidth(Double.MAX_VALUE);
        btnRemoveBook.setAlignment(Pos.BASELINE_LEFT);
        btnRemoveBook.setOnAction(e -> {
             try {

                removeBook();

             } catch (Exception ex) {}

        });

        ImageView exitIcon = new ImageView(new Image(new FileInputStream("icons/exit.png")));
        exitIcon.setFitHeight(25);
        exitIcon.setFitWidth(25);
        Button btnLogout = new Button("EXIT",exitIcon);
        btnLogout.setMaxWidth(Double.MAX_VALUE);
        btnLogout.setAlignment(Pos.BASELINE_LEFT);
        btnLogout.setOnAction(e -> {

            AlertBox alert = new AlertBox();
            alert.start(AlertBox.classStage);
        });

        buttonBox.getChildren().addAll(btnViewBooks, btnAddBook, btnAddMember, btnviewCustomer, btnIssueBook, btnViewIssuedeBook, btnRemoveBook, btnLogout);

        //Add VBox to leftLayout.
        leftLayout.setCenter(buttonBox);

        //Place into Application.
        layout.setLeft(leftLayout);

    }

    /**
     * buildTop. Create a Title Bar.
     *
     */

    private void buildTop() {

        BorderPane topLayout = new BorderPane();
        topLayout.setId("topLayoutContainer");

        Label divider = new Label();
        divider.setId("divider2");
        divider.setMaxHeight(1);
        divider.setMinHeight(1);
        divider.setMinWidth(Screen.getPrimary().getBounds().getWidth());
        topLayout.setBottom(divider);

        //Create an HBox to hold title.
        HBox titleBox = new HBox();
        titleBox.setAlignment(Pos.TOP_LEFT);
        titleBox.setSpacing(150);
        titleBox.setId("titleBox");

        //Create title.
        ImageView bookIcon = null;
        try {
            bookIcon = new ImageView(new Image(new FileInputStream("icons/books.png")));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        bookIcon.setFitHeight(25);
        bookIcon.setFitWidth(35);
        Label title = new Label("Library Management System", bookIcon);
        title.setId("appTitle");
        titleBox.getChildren().add(title);

        HBox hBox = new HBox();
        hBox.setSpacing(10);
        searchField = new TextField();
        searchField.setPromptText("Enter Book Title");
        hBox.getChildren().add(searchField);

        ImageView searchBookIcon = null;
        try {
            searchBookIcon = new ImageView(new Image(new FileInputStream("icons/searchBook.png")));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        searchBookIcon.setFitHeight(20);
        searchBookIcon.setFitWidth(25);
        Button buttonSearch = new Button("Search", searchBookIcon);
        buttonSearch.setOnAction(e -> {

            try {
                validateSearchTitle();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });

        searchField2 = new TextField();
        searchField2.setPromptText("Enter Customer ID");

        ImageView searchMemberIcon = null;
        try {
            searchMemberIcon = new ImageView(new Image(new FileInputStream("icons/searchMember.png")));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        searchMemberIcon.setFitHeight(20);
        searchMemberIcon.setFitWidth(25);
        Button buttonSearch2 = new Button("Search", searchMemberIcon);
        buttonSearch2.setOnAction(e -> {

            try {
                validateSearchId();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });
        hBox.getChildren().add(buttonSearch);

        hBox.getChildren().add(searchField2);

        hBox.getChildren().add(buttonSearch2);

        titleBox.getChildren().add(hBox);

        topLayout.setCenter(titleBox);
        layout.setTop(topLayout);

    }

    /**
     * setSceneProperties. This simply sets app to almost full size.
     * It also is where the css stylesheet is attached to app.
     */

    private void setSceneProperties() {
        //The percentage values are used as multipliers for screen width/height.
        double percentageWidth = 0.98;
        double percentageHeight = 0.90;

        //Calculate the width / height of screen.
        Rectangle2D screenSize = Screen.getPrimary().getBounds();
        percentageWidth *= screenSize.getWidth();
        percentageHeight *= screenSize.getHeight();

        //Create a scene object. Pass in the layout and set
        //the dimensions to 98% of screen width & 90% screen height.
        this.scene = new Scene(layout, percentageWidth, percentageHeight);

        //Add CSS Style Sheet
        String css = this.getClass().getResource("Demo1.css").toExternalForm();
        scene.getStylesheets().add(css);

    }

    /**
     * A {@link TableView} is made up of a number of TableColumn instances. Each
     * TableColumn in a table is responsible for displaying (and editing) the contents
     * of that column.
     *
     * It is hoped that over time there will be convenience cell value factories
     * developed and made available to developers. As of the JavaFX 2.0 release,
     * there is one such convenience class: {@link PropertyValueFactory}. This class
     * removes the need to write the code above, instead relying on reflection to
     * look up a given property from a String. Refer to the
     * <code>PropertyValueFactory</code> class documentation for more information
     * on how to use this with a TableColumn.
     *
     * */

    private void table(){

        TableColumn<Book, String> titleColumn = new TableColumn("Title");
        titleColumn.setCellValueFactory(new PropertyValueFactory<>("title"));

        TableColumn<Book, Integer> numberColumn = new TableColumn("ISBN");
        numberColumn.setCellValueFactory(new PropertyValueFactory<>("isbn"));

        TableColumn<Book, String> authorColumn = new TableColumn("Author Name");
        authorColumn.setCellValueFactory(new PropertyValueFactory<>("authorName"));

        TableColumn<Book, Double> editionColumn = new TableColumn("Edition");
        editionColumn.setCellValueFactory(new PropertyValueFactory<>("edition"));

        TableColumn<Book, String> categoryColumn = new TableColumn("Category");
        categoryColumn.setCellValueFactory(new PropertyValueFactory<>("category"));

        TableColumn<Book, Integer> idColumn = new TableColumn<>("ID");
        idColumn.setCellValueFactory(new PropertyValueFactory<>("BookID"));

        tableBook = new TableView<>();
        getTable();
        tableBook.getColumns().addAll(titleColumn, numberColumn, authorColumn, editionColumn, categoryColumn, idColumn);
    }

    private void viewBook() {

        //Create a container to fill 100% space in Center Region of
        //App BorderPane (layout).
        VBox exContainer = new VBox();
        exContainer.setId("exContainer");

        listWindow = new Stage();
        listWindow.setTitle("List of Books");

        table();
        VBox box = new VBox();
        box.getChildren().add(tableBook);

        BorderPane Layout = new BorderPane();
        Layout.setCenter(tableBook);

        Scene scene1 = new Scene(Layout);
        listWindow.setScene(scene1);
        exContainer.getChildren().add(box);
        listWindow.show();

        //return exContainer;
    }

    /**
     * Reads text from a character-input stream, buffering characters so as to
     * provide for the efficient reading of characters, arrays, and lines.
     *
     * <p> The buffer size may be specified, or the default size may be used.  The
     * default is large enough for most purposes.
     *
     * <p> In general, each read request made of a Reader causes a corresponding
     * read request to be made of the underlying character or byte stream.  It is
     * therefore advisable to wrap a BufferedReader around any Reader whose read()
     * operations may be costly, such as FileReaders and InputStreamReaders.  For
     * example,
     *
     * <pre>
     * BufferedReader in
     *   = new BufferedReader(new FileReader("foo.in"));
     * </pre>
     *
     * will buffer the input from the specified file.  Without buffering, each
     * invocation of read() or readLine() could cause bytes to be read from the
     * file, converted into characters, and then returned, which can be very
     * inefficient.
     *
     * <p> Programs that use DataInputStreams for textual input can be localized by
     * replacing each DataInputStream with an appropriate BufferedReader.
     *
     * @see FileReader
     * @see InputStreamReader
     * @see java.nio.file.Files#newBufferedReader
     *
     */

    private static int count() {

        int count = 0;
        String line;
        try {
            BufferedReader br = new BufferedReader(new FileReader(new File("Books.txt")));
            while ((line = br.readLine()) != null) {
                count++;
            }
        } catch (Exception e) {}
        return count;
    }

    private void getTable() {

          Book[] books = new Book[count()];
          books = popArray();

          for (int i = 0; i < count(); i++) {

              tableBook.getItems().add((new Book(books[i].getTitle(),books[i].getIsbn(),books[i].getAuthorName(),books[i].getEdition(),books[i].getCategory(),books[i].getBookID())));

          }
    }

    public static Book[] populateArray() {

        Book[] books = new Book[count()];

        try {
            BufferedReader br = new BufferedReader(new FileReader(new File("Books.txt")));
            String line;
            String[] array;

            int lines = 0;
            int i = 0;

                while ((line = br.readLine()) != null) {

                    array = line.split("-");

                        books[i] = new Book();
                        books[i].setTitle(array[0]);
                        books[i].setIsbn(Integer.parseInt(array[1]));
                        books[i].setAuthorName(array[2]);
                        books[i].setEdition(Double.parseDouble(array[3]));
                        books[i].setCategory(array[4]);
                        books[i].setBookID(Integer.parseInt(array[5]));
                        i++;
                    }

                br.close();

            } catch (IOException e) {

                }

        books = sorting(books);

        return books;
    }

    public static Book[] sorting(Book[] books) {

        //creating temporary variables to allow swapping of data if not sorted
        String tempTitle, tempAuthorName, tempCategory;
        int tempIsbn, tempQuantity;
        double tempEdition;

        for (int x = 1; x < count(); x++) {

            for (int j = x; j > 0; j--) {

                if (books[j].getTitle().compareToIgnoreCase(books[j - 1].getTitle()) < 0) {

                    tempTitle = books[j].getTitle();
                    tempIsbn = books[j].getIsbn();
                    tempAuthorName = books[j].getAuthorName();
                    tempEdition = books[j].getEdition();
                    tempCategory = books[j].getCategory();
                    tempQuantity = books[j].getBookID();

                    books[j].setTitle(books[j - 1].getTitle());
                    books[j].setIsbn(books[j - 1].getIsbn());
                    books[j].setAuthorName(books[j - 1].getAuthorName());
                    books[j].setEdition(books[j - 1].getEdition());
                    books[j].setCategory(books[j - 1].getCategory());
                    books[j].setBookID(books[j - 1].getBookID());

                    books[j - 1].setTitle(tempTitle);
                    books[j - 1].setIsbn(tempIsbn);
                    books[j - 1].setAuthorName(tempAuthorName);
                    books[j - 1].setEdition(tempEdition);
                    books[j - 1].setCategory(tempCategory);
                    books[j - 1].setBookID(tempQuantity);
                }
            }
        }

        return books;
    }

    private void buttonAdd() {

        Stage addNewBook = new Stage();
        addNewBook.setOnCloseRequest(e -> {
            stage.close();
            Platform.runLater(() -> new Main_Menu().start(new Stage()));
        });

        Label title = new Label();
        title.setText("Add New Book");
        title.setId("title");

        GridPane titleBox = new GridPane();
        titleBox.add(title, 0, 0);

        Text text1 = new Text("Enter Book Title");
        titleField = new TextField();

        Text text2 = new Text("Enter Book Number");
        numField = new TextField();

        Text text3 = new Text("Enter Author Name");
        authorField = new TextField();

        Text text4 = new Text("Enter Edition");
        editionField = new TextField();

        Text text6 = new Text("ID");
        idField = new TextField();

        Text text5 = new Text("Select Category");
        comboBox = new ComboBox();
        comboBox.getItems().addAll(

                "Action and adventure",
                "Art",
                "Children's literature",
                "Comic book",
                "Cookbook",
                "Crime",
                "Dictionary",
                "Drama",
                "Encyclopedia",
                "Fantasy",
                "Health",
                "History",
                "Poetry",
                "Romance",
                "Science",
                "Science Fiction",
                "Technology",
                "Travel"
        );

        Button button1 = new Button("Add");
        button1.setOnAction(e -> {

            try {
                validateAdd();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });

        Button btnCancel = new Button("Cancel");
        btnCancel.setOnAction(e -> {

            addNewBook.close();
        });

        GridPane gridPane = new GridPane();
        gridPane.setMinSize(400, 200);
        gridPane.setPadding(new Insets(10, 10, 10, 10));
        gridPane.setVgap(5);
        gridPane.setHgap(5);
        gridPane.setAlignment(Pos.CENTER);

        gridPane.getChildren().add(titleBox);

        gridPane.add(text1, 0, 1);
        gridPane.add(titleField, 1, 1);

        gridPane.add(text2, 0, 2);
        gridPane.add(numField, 1, 2);

        gridPane.add(text3, 0, 3);
        gridPane.add(authorField, 1, 3);

        gridPane.add(text4, 0, 4);
        gridPane.add(editionField, 1, 4);

        gridPane.add(text6, 0, 5);
        gridPane.add(idField, 1, 5);

        gridPane.add(text5, 0, 6);
        gridPane.add(comboBox, 1, 6);

        gridPane.add(button1, 0, 7);
        gridPane.add(btnCancel, 1, 7);

        Scene scene = new Scene(gridPane);
        addNewBook.setScene(scene);
        addNewBook.show();
    }

    public void validateAdd() throws Exception {

        String inputTitle = titleField.getText();
        String inputNumber = numField.getText();
        String inputName = authorField.getText();
        String inputEdition = editionField.getText();
        String inputId = idField.getText();

        String validString = "^[a-zA-Z]+$";
        String validInt = "^[0-9.]+$";

        if(inputTitle.equalsIgnoreCase("")) {

            JOptionPane.showMessageDialog(null, "Enter Book Tilte!");
            return;
        }

        else if(!inputTitle.matches(validString)) {

            JOptionPane.showMessageDialog(null, "Check Book Title!!!!");
            return;
        }

        else if(inputNumber.equalsIgnoreCase("")) {

            JOptionPane.showMessageDialog(null, "Enter Book Number(ISBN)!");
            return;
        }

        else if(!inputNumber.matches(validInt) || inputNumber.equalsIgnoreCase("0")) {

            JOptionPane.showMessageDialog(null, "Check Book Number!!!!");
            return;
        }

        else if(inputName.equalsIgnoreCase("")) {

            JOptionPane.showMessageDialog(null, "Enter Author Name!");
            return;
        }

        else if(!inputName.matches(validString)) {

            JOptionPane.showMessageDialog(null, "Check name!!!!");
            return;
        }

        else if(inputEdition.equalsIgnoreCase("")) {

            JOptionPane.showMessageDialog(null, "Enter Book Edtion!");
            return;
        }

        else if(!inputEdition.matches(validInt) || inputEdition.equalsIgnoreCase("0")) {

            JOptionPane.showMessageDialog(null, "Check Book Edition!!!!");
            return;
        }

        else if(inputId.equalsIgnoreCase("")) {

            JOptionPane.showMessageDialog(null, "Enter Book Edtion!");
            return;
        }

        else if(!inputId.matches(validInt) || inputId.equalsIgnoreCase("0")) {

            JOptionPane.showMessageDialog(null, "Check Book Edition!!!!");
            return;
        }

        int number =  Integer.parseInt(inputNumber);
        double edition = Double.parseDouble(inputEdition);
        int id =  Integer.parseInt(inputId);

        add(inputTitle, number, inputName, edition, id);

    }

    private void add(String inputTitle, int number, String inputName, double edition, int id) {

        try {

        File inputFile = new File("Books.txt");

        BufferedWriter bw = new BufferedWriter(new FileWriter(inputFile, true));

        bw.newLine();
        bw.write(inputTitle + "-" + number + "-" + inputName + "-" + edition + "-"  + comboBox.getValue() + "-" + id);

        bw.close();

        JOptionPane.showMessageDialog(null, "Book Added Successfully");

        titleField.clear();
        numField.clear();
        authorField.clear();
        editionField.clear();
        idField.clear();
        comboBox.getSelectionModel().clearSelection();

        tableBook.getItems().clear();
        getTable();

        } catch (IOException e) {

        }
    }

    private void searchTitle(String Inputsearch) throws Exception {

        Book[] books = popArray();
        int search = -1;

        for(int i = 0; i < count(); i++) {

            if(Inputsearch.equalsIgnoreCase(books[i].getTitle())) {

                search = i;
            }
        }

        if(search == -1) {

            JOptionPane.showMessageDialog(null,"No book found!!!");

        }

        else

            JOptionPane.showMessageDialog(null, "BOOK FOUND\n" +
                    "Author: " + books[search].getAuthorName() + "\n" +
                    "Book Number: " + books[search].getIsbn() + "\n" +
                    "Edition: " + books[search].getEdition() + "\n" +
                    "Book ID: " + books[search].getBookID());
            searchField.clear();
    }

    private void searchId(int clientId) throws Exception {

        Client[] clients = populateArray();

        int result = -1;

        for(int i = 0; i < clientCount(); i++) {

            if(clientId == clients[i].getNid()) {

                result = i;
            }
        }

        if(result == -1) {

            JOptionPane.showMessageDialog(null,"No Customer Found!!!");

        }

        else

            JOptionPane.showMessageDialog(null, "Customer FOUND\n" +
                    "Name: " + clients[result].getName() + "\n" +
                    "Family Name: " + clients[result].getSurName() + "\n" +
                    "Phone: " + clients[result].getPhoneNumber() + "\n" +
                    "Email: " + clients[result].getEmails());
        searchField.clear();

    }

    public void validateSearchTitle() throws Exception {

        String InputTitle = searchField.getText();

        String validString = "^[a-z A-Z]+$";

        if(InputTitle.equalsIgnoreCase("")) {

            JOptionPane.showMessageDialog(null, "Enter Book Title!");
            return;
        }

        else if(!InputTitle.matches(validString)) {

            JOptionPane.showMessageDialog(null, "Enter Valid Book Title!");
            return;
        }

        searchTitle(InputTitle);

    }

    public void validateSearchId() throws  Exception {

        String InputId = searchField2.getText();
        String validInt = "^[0-9.]+$";

         if(InputId.equalsIgnoreCase("")) {

            JOptionPane.showMessageDialog(null, "Enter ID Number!");
            return;
        }

        else if(!InputId.matches(validInt) || InputId.equalsIgnoreCase("0")) {

            JOptionPane.showMessageDialog(null, "Check ID Number!!!!");
            return;
        }

        int clientId = Integer.parseInt(InputId);
        searchId(clientId);
    }

    private void addClient() {

        Stage rent_Book = new Stage();
        rent_Book.setOnCloseRequest(e -> {
            stage.close();
            Platform.runLater(() -> new Main_Menu().start(new Stage()));
        });

        Label title = new Label();
        title.setText("Client Details");

        GridPane titleBox = new GridPane();
        titleBox.add(title, 0, 0);

        Label name = new Label();
        name.setText("Enter First Name");
        firstNameField = new TextField();

        Label surname = new Label();
        surname.setText("Enter Family Name");
        surNameField = new TextField();

        Label phone = new Label();
        phone.setText("Phone Number");
        phoneField = new TextField();

        Label email = new Label();
        email.setText("Email");
        mailField = new TextField();

        Label nid = new Label();
        nid.setText("ID");
        nidField = new TextField();

        Button button1 = new Button("ENTER");
        button1.setOnAction(e -> {

            try {
                validateClientInput();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });

        Button btnCancel = new Button("Cancel");
        btnCancel.setOnAction(e -> {

            rent_Book.close();

        });

        GridPane gridPane = new GridPane();
        gridPane.setMinSize(300, 300);
        gridPane.setPadding(new Insets(10, 10, 10, 10));
        gridPane.setVgap(5);
        gridPane.setHgap(5);

        gridPane.getChildren().add(titleBox);

        gridPane.add(name, 0, 1);
        gridPane.add(firstNameField, 1, 1);

        gridPane.add(surname, 0, 2);
        gridPane.add(surNameField, 1, 2);

        gridPane.add(phone,0,3);
        gridPane.add(phoneField, 1, 3);

        gridPane.add(email, 0, 4);
        gridPane.add(mailField, 1, 4);

        gridPane.add(nid,0, 5);
        gridPane.add(nidField, 1, 5);

        gridPane.add(button1, 0, 8);
        gridPane.add(btnCancel, 1, 8);

        Scene scene = new Scene(gridPane);
        rent_Book.setScene(scene);
        rent_Book.show();
    }

    public void validateClientInput() throws Exception {

        String firstName = firstNameField.getText();
        String surName = surNameField.getText();
        String phone = phoneField.getText();
        String mail = mailField.getText();
        String nid = nidField.getText();

        String validString = "^[a-zA-Z]+$";
        String validInt = "^[0-9.]+$";
        String validMail = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";

        if(firstName.equalsIgnoreCase("")) {

            JOptionPane.showMessageDialog(null, "Enter Customer Name!");
            return;
        }

        else if(!firstName.matches(validString)) {

            JOptionPane.showMessageDialog(null, "Enter Valid Customer Name!");
            return;
        }

        else if(surName.equalsIgnoreCase("")) {

            JOptionPane.showMessageDialog(null, "Enter Customer Family Name!");
            return;
        }

        else if(!surName.matches(validString)) {

            JOptionPane.showMessageDialog(null, "Enter Valid Customer Family Name!");
            return;
        }

        else if(phone.equalsIgnoreCase("")) {

            JOptionPane.showMessageDialog(null, "Enter Phone Number!");
            return;
        }

        else if(!phone.matches(validInt) || phone.equalsIgnoreCase("0")) {

            JOptionPane.showMessageDialog(null, "Check Phone Number!!!!");
            return;
        }

        else if(mail.equalsIgnoreCase("")) {

            JOptionPane.showMessageDialog(null, "Enter email!");
            return;
        }

        else if(!mail.matches(validMail)) {

            JOptionPane.showMessageDialog(null, "Enter a Valid Email!");
            return;
        }

        else if(nid.equalsIgnoreCase("")) {

            JOptionPane.showMessageDialog(null, "Enter customer NID!");
            return;
        }

        else if(!nid.matches(validInt) || nid.equalsIgnoreCase("0")) {

            JOptionPane.showMessageDialog(null, "Check coutomer NID!!!!");
            return;
        }

        int phoneNumber = Integer.parseInt(phone);
        int Clientnid = Integer.parseInt(nid);

        storeClientDetails(firstName, surName, phoneNumber, mail, Clientnid);
    }

    private void storeClientDetails(String firstName, String surName, int phoneNumber, String mail, int Clientnid) {

        try {

            File inputFile = new File("Clients.txt");

            BufferedWriter bw = new BufferedWriter(new FileWriter(inputFile, true));

            bw.newLine();
            bw.write(firstName + "-" + surName + "-" + phoneNumber + "-" + mail + "-" + Clientnid);

            bw.close();

            firstNameField.clear();
            surNameField.clear();
            phoneField.clear();
            mailField.clear();
            nidField.clear();

            tableClient.getItems().clear();
            getClientTable();

        } catch (IOException e) {

        }
    }

    public void clientTable() {

        TableColumn<Client, String> nameColumn = new TableColumn("Name");
        nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));

        TableColumn<Client, String> surnameColumn = new TableColumn("Family Name");
        surnameColumn.setCellValueFactory(new PropertyValueFactory<>("surName"));

        TableColumn<Client, Integer> phoneColumn = new TableColumn("Phone");
        phoneColumn.setCellValueFactory(new PropertyValueFactory<>("phoneNumber"));

        TableColumn<Client, String> mailColumn = new TableColumn("Emails");
        mailColumn.setCellValueFactory(new PropertyValueFactory<>("emails"));

        TableColumn<Client, Integer> idColumn = new TableColumn("NID");
        idColumn.setCellValueFactory(new PropertyValueFactory<>("nid"));

        tableClient = new TableView<>();
        getClientTable();
        tableClient.getColumns().addAll(nameColumn, surnameColumn, phoneColumn, mailColumn, idColumn);

    }

    private void viewClient() {

        VBox container = new VBox();

        listClient = new Stage();
        listClient.setTitle("Customer List");

        clientTable();
        VBox box = new VBox();
        box.getChildren().add(tableClient);

        BorderPane window = new BorderPane();
        window.setCenter(tableClient);

        Scene scene2 = new Scene(window);
        listClient.setScene(scene2);
        container.getChildren().add(box);
        listClient.show();

    }

    private static int clientCount() {

        int count = 0;
        String line;

        try {

            BufferedReader br = new BufferedReader(new FileReader(new File("Clients.txt")));

            while ((line = br.readLine()) != null) {

                count++;
            }

        } catch (Exception e) {}

        return count;
    }

    private void getClientTable() {

        Client[] clients = new Client[clientCount()];
        clients = populateArray();

        for (int i = 0; i < clientCount(); i++) {

            tableClient.getItems().add((new Client(clients[i].getName(),clients[i].getSurName(),clients[i].getPhoneNumber(),clients[i].getEmails(),clients[i].getNid())));
        }
    }

    public Client[] populateArray() {

        Client[] clients = new Client[clientCount()];

        try {
            BufferedReader br = new BufferedReader(new FileReader(new File("Clients.txt")));
            String line;
            String[] array;

            int lines = 0;
            int i = 0;

            while ((line = br.readLine()) != null) {

                array = line.split("-");

                clients[i] = new Client();
                clients[i].setName(array[0]);
                clients[i].setSurName(array[1]);
                clients[i].setPhoneNumber(Integer.parseInt(array[2]));
                clients[i].setEmails(array[3]);
                clients[i].setNid(Integer.parseInt(array[4]));
                i++;
            }

            br.close();

        } catch (IOException e) {}

        clients = sort(clients);

        return clients;
    }

    public static Client[] sort(Client[] clients) {

        String tempName, tempSurName, tempEmails;
        int tempPhoneNumber, tempNid;

        for (int a = 1; a < clientCount(); a++) {

            for (int k = a; k > 0; k--) {

                if (clients[k].getName().compareToIgnoreCase(clients[k - 1].getName()) < 0) {

                    tempName = clients[k].getName();
                    tempSurName = clients[k].getSurName();
                    tempPhoneNumber = clients[k].getPhoneNumber();
                    tempEmails = clients[k].getEmails();
                    tempNid = clients[k].getNid();

                    clients[k].setName(clients[k - 1].getName());
                    clients[k].setSurName(clients[k - 1].getSurName());
                    clients[k].setPhoneNumber(clients[k - 1].getPhoneNumber());
                    clients[k].setEmails(clients[k - 1].getEmails());
                    clients[k].setNid(clients[k - 1].getNid());

                    clients[k - 1].setName(tempName);
                    clients[k - 1].setSurName(tempSurName);
                    clients[k - 1].setPhoneNumber(tempPhoneNumber);
                    clients[k - 1].setEmails(tempEmails);
                    clients[k - 1].setNid(tempNid);
                }
            }
        }

        return clients;
    }

    private void issueBookFromSearch(int result, int resultName) throws Exception {

        Book[] books = popArray();
        Client[] clients = populateArray();

        Stage issue_Book = new Stage();
        issue_Book.setOnCloseRequest(e -> {
            stage.close();
            Platform.runLater(() -> new Main_Menu().start(new Stage()));
        });

        Label title = new Label();
        title.setText("Issue Book");

        GridPane titleBox = new GridPane();
        titleBox.add(title, 0, 0);

        Label bookid = new Label();
        bookid.setText("Book ID");
        bookIDfield = new TextField();

        Label id = new Label();
        id.setText("Member ID");
        memberIDfield = new TextField();

        Button btnfind = new Button();
        btnfind.setText("Find");

        Label titleBook = new Label();
        titleBook.setText("Book Title");
        bookTitlefield = new TextField();
        bookTitlefield.setText(books[result].getTitle());
        bookTitlefield.setEditable(false);

        Label name = new Label();
        name.setText("Member Name");
        memberName = new TextField();
        memberName.setText(clients[resultName].getName());
        memberName.setEditable(false);

        Label date = new Label();
        date.setText("Date Of Issue");
        day = new ComboBox();
        day.getItems().addAll(

                "1",
                "2",
                "3",
                "4",
                "5",
                "6",
                "7",
                "8",
                "9",
                "10",
                "11",
                "12",
                "13",
                "14",
                "15",
                "16",
                "17",
                "18",
                "19",
                "20",
                "21",
                "22",
                "23",
                "24",
                "25",
                "26",
                "27",
                "28",
                "29",
                "30",
                "31"
        );

        month = new ComboBox();
        month.getItems().addAll(

                "January",
                "February",
                "March",
                "April",
                "May",
                "June",
                "July",
                "August",
                "September",
                "October",
                "November",
                "December"
        );

        year = new ComboBox();
        year.getItems().addAll(

                "2019",
                "2020",
                "2021"
        );

        Button button1 = new Button("ENTER");
        button1.setOnAction(e -> {

            try {
                validateIssueBook();
            } catch (Exception ex) {
                ex.printStackTrace();
            }

        });

        Button btnCancel = new Button("Cancel");
        btnCancel.setOnAction(e -> {

            issue_Book.close();
        });

        GridPane gridPane = new GridPane();
        gridPane.setMinSize(300, 300);
        gridPane.setPadding(new Insets(10, 10, 10, 10));
        gridPane.setVgap(20);
        gridPane.setHgap(5);

        gridPane.getChildren().add(titleBox);

        gridPane.add(bookid, 0, 1);
        gridPane.add(bookIDfield, 1, 1);

        gridPane.add(id, 0, 2);
        gridPane.add(memberIDfield, 1, 2);

        gridPane.add(btnfind, 0,3);

        gridPane.add(titleBook, 0, 5);
        gridPane.add(bookTitlefield, 1, 5);

        gridPane.add(name, 0, 6);
        gridPane.add(memberName, 1, 6);

        gridPane.add(date, 0, 7);
        gridPane.add(day, 1, 7);
        gridPane.add(month, 2, 7);
        gridPane.add(year, 3, 7);

        gridPane.add(button1, 0, 8);
        gridPane.add(btnCancel, 1, 8);

        Scene scene = new Scene(gridPane);
        issue_Book.setScene(scene);
        issue_Book.show();
    }

    private void issueBook() throws Exception {

        Book[] books = popArray();
        Client[] clients = populateArray();

        Stage issue_Book = new Stage();
        issue_Book.setOnCloseRequest(e -> {
            stage.close();
            Platform.runLater(() -> new Main_Menu().start(new Stage()));
        });

        Label title = new Label();
        title.setText("Issue Book");

        GridPane titleBox = new GridPane();
        titleBox.add(title, 0, 0);

        Label bookid = new Label();
        bookid.setText("Book ID");
        bookIDfield = new TextField();

        Label id = new Label();
        id.setText("Member ID");
        memberIDfield = new TextField();

        Button btnfind = new Button();
        btnfind.setText("Find");
        btnfind.setOnAction(e -> {

            try {
                validateFind();
                issue_Book.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });

        Label titleBook = new Label();
        titleBook.setText("Book Title");
        bookTitlefield = new TextField();
        bookTitlefield.setText("");
        bookTitlefield.setEditable(false);

        Label name = new Label();
        name.setText("Member Name");
        memberName = new TextField();
        memberName.setText("");
        memberName.setEditable(false);

        Label date = new Label();
        date.setText("Date Of Issue");
        day = new ComboBox();
        day.getItems().addAll(

                "1",
                "2",
                "3",
                "4",
                "5",
                "6",
                "7",
                "8",
                "9",
                "10",
                "11",
                "12",
                "13",
                "14",
                "15",
                "16",
                "17",
                "18",
                "19",
                "20",
                "21",
                "22",
                "23",
                "24",
                "25",
                "26",
                "27",
                "28",
                "29",
                "30",
                "31"
        );

        month = new ComboBox();
        month.getItems().addAll(

                "January",
                "February",
                "March",
                "April",
                "May",
                "June",
                "July",
                "August",
                "September",
                "October",
                "November",
                "December"
        );

        year = new ComboBox();
        year.getItems().addAll(

                "2019",
                "2020",
                "2021"
        );

        Button button1 = new Button("ENTER");
        button1.setOnAction(e -> {

            try {
                validateIssueBook();
            } catch (Exception ex) {
                ex.printStackTrace();
            }

        });

        Button btnCancel = new Button("Cancel");
        btnCancel.setOnAction(e -> {

            issue_Book.close();

        });

        GridPane gridPane = new GridPane();
        gridPane.setMinSize(300, 300);
        gridPane.setPadding(new Insets(10, 10, 10, 10));
        gridPane.setVgap(20);
        gridPane.setHgap(5);

        gridPane.getChildren().add(titleBox);

        gridPane.add(bookid, 0, 1);
        gridPane.add(bookIDfield, 1, 1);

        gridPane.add(id, 0, 2);
        gridPane.add(memberIDfield, 1, 2);

        gridPane.add(btnfind, 0,3);

        gridPane.add(titleBook, 0, 5);
        gridPane.add(bookTitlefield, 1, 5);

        gridPane.add(name, 0, 6);
        gridPane.add(memberName, 1, 6);

        gridPane.add(date, 0, 7);
        gridPane.add(day, 1, 7);
        gridPane.add(month, 2, 7);
        gridPane.add(year, 3, 7);

        gridPane.add(button1, 0, 8);
        gridPane.add(btnCancel, 1, 8);

        Scene scene = new Scene(gridPane);
        issue_Book.setScene(scene);
        issue_Book.show();
    }

    public void validateIssueBook() throws Exception {

        String bookTitle = bookTitlefield.getText();
        String customerName = memberName.getText();
        String inputDay = day.getValue();
        String inputMonth = month.getValue();
        String inputYear = year.getValue();

        String validString = "^[a-zA-Z]+$";
        String validInt = "^[0-9.]+$";


        String newdate;
        if (bookTitle.equalsIgnoreCase("")) {

            JOptionPane.showMessageDialog(null, "Field cannot be empty...Please find a book!!!!");
            return;

        } else if (customerName.equalsIgnoreCase("")) {

            JOptionPane.showMessageDialog(null, "Field cannot be empty...Please find customer Name!!!!");
            return;

        } else if (inputDay.equalsIgnoreCase("")) {

            JOptionPane.showMessageDialog(null, "Please select A Day");
            return;
        }

        else if (inputMonth.equalsIgnoreCase("")) {

            JOptionPane.showMessageDialog(null, "Please select A Month");
            return;
        }

        else if (inputYear.equalsIgnoreCase("")) {

            JOptionPane.showMessageDialog(null, "Please select A Year");
            return;
        }


        storeIssueBook(bookTitle, customerName, inputDay, inputMonth, inputYear);
    }

    public void find(int bookId, int memberId) throws Exception {

        Book[] books = popArray();
        int resultBook = -1;
        Client[] clients = populateArray();
        int resultName = -1;

        for(int i = 0; i < count(); i++) {

            if(books[i].getBookID() == bookId) {

                resultBook = i;
            }
        }

        for (int x = 0; x < clientCount(); x++) {

                if (clients[x].getNid() == memberId) {

                    resultName = x;
                }
            }

        if(resultBook == -1) {

            JOptionPane.showMessageDialog(null,"No book found!!!");
            return;
        }

            if (resultName == -1) {

                JOptionPane.showMessageDialog(null, "No Member Found!!!");
                return;
            }

                issueBookFromSearch(resultBook, resultName);
    }

    public void validateFind() throws Exception {

        String inputBookId = bookIDfield.getText();
        String inputMemberId = memberIDfield.getText();

        String validString = "^[a-zA-Z]+$";
        String validInt = "^[0-9.]+$";

        if(inputBookId.equalsIgnoreCase("")) {

            JOptionPane.showMessageDialog(null, "Enter Book ID!");
            return;
        }

        else if(!inputBookId.matches(validInt) || inputBookId.equalsIgnoreCase("0")) {

            JOptionPane.showMessageDialog(null, "Check Book ID!!!!");
            return;
        }

        else if(inputMemberId.equalsIgnoreCase("")) {

            JOptionPane.showMessageDialog(null, "Enter member ID!");
            return;
        }

        else if(!inputMemberId.matches(validInt) || inputMemberId.equalsIgnoreCase("0")) {

            JOptionPane.showMessageDialog(null, "Check member ID!!!!");
            return;
        }

        int bookId = Integer.parseInt(inputBookId);
        int memberId = Integer.parseInt(inputMemberId);

        find(bookId, memberId);
    }

    private void storeIssueBook(String bookTitle, String customerName, String inputDay, String inputMonth, String inputYear) throws Exception {

        try {

            File inputFile = new File("Issued_Book.txt");

            BufferedWriter bw = new BufferedWriter(new FileWriter(inputFile, true));

            bw.newLine();
            bw.write(bookTitle + "-" + customerName + "-" + inputDay + "-" + inputMonth + "-" + inputYear);

            bw.close();

            bookTitlefield.clear();
            memberName.clear();

            tableIssue.getItems().clear();
            getIssuedTable();

        } catch (IOException e) {}
    }

    private static int issuedCount() {

        int count = 0;
        String line;

        try {

            BufferedReader br = new BufferedReader(new FileReader(new File("Issued_Book.txt")));

            while ((line = br.readLine()) != null) {

                count++;
            }

        } catch (Exception e) {}

        return count;
    }

    public Issued[] populate() {

        Issued[] issue = new Issued[issuedCount()];

        try {
            BufferedReader br = new BufferedReader(new FileReader(new File("Issued_Book.txt")));
            String line;
            String[] array;

            int lines = 0;
            int i = 0;

            while ((line = br.readLine()) != null) {

                array = line.split("-");

                issue[i] = new Issued();
                issue[i].setTitle(array[0]);
                issue[i].setName(array[1]);
                issue[i].setDay(array[2]);
                issue[i].setMonth(array[3]);
                issue[i].setYear(array[4]);
                i++;
            }

            br.close();

        } catch (IOException e) {}

        return issue;

    }

    private void getIssuedTable() {

        Issued[] issue = new Issued[issuedCount()];
        issue = populate();

        for (int i = 0; i < issuedCount(); i++) {

            tableIssue.getItems().add((new Issued(issue[i].getTitle(),issue[i].getName(),issue[i].getDay(), issue[i].getMonth(), issue[i].getYear())));
        }
    }

    private void issuedTable() {

        TableColumn<Issued, String> titleColumn = new TableColumn("Book Title");
        titleColumn.setCellValueFactory(new PropertyValueFactory<>("title"));

        TableColumn<Issued, String> memberColumn = new TableColumn("Issued to");
        memberColumn.setCellValueFactory(new PropertyValueFactory<>("name"));

        TableColumn<Issued, String> issuedDayColumn = new TableColumn("Issued Day");
        issuedDayColumn.setCellValueFactory(new PropertyValueFactory<>("day"));

        TableColumn<Issued, String> issuedMonthColumn = new TableColumn("Issued Month");
        issuedMonthColumn.setCellValueFactory(new PropertyValueFactory<>("month"));

        TableColumn<Issued, String> issuedYearColumn = new TableColumn("Issued Year");
        issuedYearColumn.setCellValueFactory(new PropertyValueFactory<>("year"));

        tableIssue = new TableView<>();
        getIssuedTable();
        tableIssue.getColumns().addAll(titleColumn, memberColumn, issuedDayColumn, issuedMonthColumn, issuedYearColumn);
    }

    private void viewIssuedBook() {

        VBox container = new VBox();

        listIssuedBook = new Stage();
        listIssuedBook.setTitle("Customer List");

        clientTable();
        VBox box = new VBox();
        box.getChildren().add(tableIssue);

        BorderPane window = new BorderPane();
        window.setCenter(tableIssue);

        Scene scene2 = new Scene(window);
        listIssuedBook.setScene(scene2);
        container.getChildren().add(box);
        listIssuedBook.show();
    }

    public void removeBook() throws Exception   {

        Stage removeBook = new Stage();
        removeBook.setOnCloseRequest(e -> {
            stage.close();
            Platform.runLater(() -> new Main_Menu().start(new Stage()));
        });

        Label title = new Label();
        title.setText("Remove Book");

        GridPane titleBox = new GridPane();
        titleBox.add(title, 0, 0);

        Label booktitle = new Label();
        booktitle.setText("Enter Book Title");
        newtitleField = new TextField();

        Button btnfind = new Button();
        btnfind.setText("Find");
        btnfind.setOnAction(e -> {

            String input = newtitleField.getText();
            String validString = "^[a-zA-Z]+$";

            if(input.equalsIgnoreCase("")) {

                JOptionPane.showMessageDialog(null, "Enter Book Title!");
                return;
            }

            else if(!input.matches(validString)) {

                JOptionPane.showMessageDialog(null, "Check Book Title!!!!");
                return;
            }

            else {

                try {

                findBookTitle();

            } catch (Exception ex) {
                ex.printStackTrace();
                }
            }
        });

        Button btnCancel = new Button("Cancel");
        btnCancel.setOnAction(e -> {

            removeBook.close();
        });

        GridPane gridPane = new GridPane();
        gridPane.setMinSize(200, 200);
        gridPane.setPadding(new Insets(10, 10, 10, 10));
        gridPane.setVgap(20);
        gridPane.setHgap(5);

        gridPane.getChildren().add(titleBox);

        gridPane.add(booktitle, 0, 1);
        gridPane.add(newtitleField, 1, 1);

        gridPane.add(btnfind, 0, 3);
        gridPane.add(btnCancel, 1, 3);

        Scene scene = new Scene(gridPane);
        removeBook.setScene(scene);
        removeBook.show();

    }

    public void findBookTitle() {

        Stage findBook = new Stage();
        findBook.setOnCloseRequest(e -> {
            stage.close();
            Platform.runLater(() -> new Main_Menu().start(new Stage()));
        });

        Book[] books = popArray();
        int resultBook = -1;
        String input = newtitleField.getText();

        for(int i = 0; i < count(); i++) {

            if(books[i].getTitle().equalsIgnoreCase(input)) {

                resultBook = i;
            }
        }

        if(resultBook == -1) {

            JOptionPane.showMessageDialog(null,"No book found!!!");
            return;

        } else {

            final int finalSearch = resultBook;

            Label iDtitleBook = new Label();
            iDtitleBook.setText("Book ID");
            newidField = new TextField();
            newidField.setText(String.valueOf(books[resultBook].getBookID()));
            newidField.setEditable(false);

            Label authorName = new Label();
            authorName.setText("Author Name");
            newauthorField = new TextField();
            newauthorField.setText(books[resultBook].getAuthorName());
            newauthorField.setEditable(false);

            Label edition = new Label();
            edition.setText("Edition");
            neweditionField = new TextField();
            neweditionField.setText(String.valueOf(books[resultBook].getEdition()));
            neweditionField.setEditable(false);

            Label isbn = new Label();
            isbn.setText("Edition");
            newisbn1Field = new TextField();
            newisbn1Field.setText(String.valueOf(books[resultBook].getIsbn()));
            newisbn1Field.setEditable(false);

            Button button1 = new Button("ENTER");
            button1.setOnAction(e -> {

                try {
                    remove(finalSearch);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

            });

            Button btnCancel = new Button("Cancel");
            btnCancel.setOnAction(e -> {

                findBook.close();
            });

            GridPane gridPane = new GridPane();
            gridPane.setMinSize(300, 300);
            gridPane.setPadding(new Insets(10, 10, 10, 10));
            gridPane.setVgap(20);
            gridPane.setHgap(5);

            gridPane.add(iDtitleBook, 0, 1);
            gridPane.add(newidField, 1, 1);

            gridPane.add(authorName, 0, 2);
            gridPane.add(newauthorField, 1, 2);

            gridPane.add(edition, 0, 3);
            gridPane.add(neweditionField, 1, 3);

            gridPane.add(isbn, 0, 4);
            gridPane.add(newisbn1Field, 1, 4);

            gridPane.add(button1, 0, 6);
            gridPane.add(btnCancel, 1, 6);

            Scene scene = new Scene(gridPane);
            findBook.setScene(scene);
            findBook.show();
        }
    }

    private void remove(int searchResult) throws Exception {

        Book[] books = popArray();

        books = removeTheElement(books, searchResult);

        JOptionPane.showMessageDialog(null, "Book Removed !");

        updateFile(books);

    }

    public static Book[] removeTheElement(Book[] books, int index) {

        Book[] book = new Book[books.length - 1];

        for (int x = 0, k = 0; x < books.length; x++) {

            if (x == index) {
                continue;
            }

            book[k++] = books[x];
        }

        return book;
    }

    /**
     * A <code>PrintStream</code> adds functionality to another output stream,
     * namely the ability to print representations of various data values
     * conveniently.  Two other features are provided as well.  Unlike other output
     * streams, a <code>PrintStream</code> never throws an
     * <code>IOException</code>; instead, exceptional situations merely set an
     * internal flag that can be tested via the <code>checkError</code> method.
     * Optionally, a <code>PrintStream</code> can be created so as to flush
     * automatically; this means that the <code>flush</code> method is
     * automatically invoked after a byte array is written, one of the
     * <code>println</code> methods is invoked, or a newline character or byte
     * (<code>'\n'</code>) is written.
     *
     * <p> All characters printed by a <code>PrintStream</code> are converted into
     * bytes using the platform's default character encoding.  The <code>{@link
     * PrintWriter}</code> class should be used in situations that require writing
     * characters rather than bytes.
     *
     */

    public void updateFile(Book[] books) throws Exception {

        try {

            File file = new File("Books.txt");

            try (PrintStream update = new PrintStream(file)) {

                for (int i = 0; i < books.length; i++) {

                    if (!(books[i].getTitle() == null)) {

                        update.print(books[i].getTitle());
                        update.print("-");
                        update.print(books[i].getIsbn());
                        update.print("-");
                        update.print(books[i].getAuthorName());
                        update.print("-");
                        update.print(books[i].getEdition());
                        update.print("-");
                        update.print(books[i].getCategory());
                        update.print("-");
                        if (i == books.length - 1) {
                            update.print(books[i].getBookID());
                        } else {
                            update.println(books[i].getBookID());
                        }
                    }
                }
                tableBook.getItems().clear();
                getTable();
            }
        } catch (FileNotFoundException e) {

            JOptionPane.showMessageDialog(null,"File not found!");
        }
    }
}
